import launch
from launch.substitutions import LaunchConfiguration
import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
import xacro


def generate_launch_description():

    ####### DATA INPUT ##########
    urdf_file = 'unicicle.urdf'
    #xacro_file = "box_bot.xacro"
    package_description = "uniclicle_robot_pkg"

    ####### DATA INPUT END ##########
    print("Fetching URDF ==>")
    robot_desc_path = os.path.join(get_package_share_directory(
        package_description), "urdf", urdf_file)

    robot_desc = xacro.process_file(robot_desc_path)
    xml = robot_desc.toxml()

    # Publish Robot Desciption in String form in the topic /robot_description
    publish_robot_description = Node(
        package="spawn_robot_tools_pkg",
        executable='robot_description_publisher_exe',
        name='unicicle_robot_description_publisher',
        output='screen',
        emulate_tty=True,
        arguments=['-xml_string', xml,
                   '-robot_description_topic', '/unicicle_bot_robot_description'
                   ],
        remappings=[("/robot_description", "/unicicle_bot_robot_description")
                    ],
    )

    # GAZEBO SPAWN
    # Position and orientation
    # [X, Y, Z]
    position = [0.0, 0.0, 0.5]
    # [Roll, Pitch, Yaw]
    orientation = [0.0, 0.0, 0.0]
    # Base Name or robot
    robot_base_name = "unicicle"

    entity_name = robot_base_name

    # Spawn ROBOT Set Gazebo
    spawn_robot = Node(
        package='gazebo_ros',
        executable='spawn_entity.py',
        name='unicicle_spawn_entity',
        output='screen',
        emulate_tty=True,
        arguments=['-entity',
                   entity_name,
                   '-x', str(position[0]), '-y', str(position[1]
                                                     ), '-z', str(position[2]),
                   '-R', str(orientation[0]), '-P', str(orientation[1]
                                                        ), '-Y', str(orientation[2]),
                   '-topic', '/unicicle_bot_robot_description'
                   ]
    )

    # create and return launch description object
    return LaunchDescription(
        [
            publish_robot_description,
            spawn_robot
        ]
    )
